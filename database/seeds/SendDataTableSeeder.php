<?php

use Illuminate\Database\Seeder;

class SendDataTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\SendData::class, 30)->create();
    }
}
