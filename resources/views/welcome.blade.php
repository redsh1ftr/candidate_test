<!DOCTYPE html>
<html>
    <head>

        <title>Ready Maker One</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="./css/main.css">
        <link rel="shortcut icon" type="image/ico" href="./favico.ico">
    </head>
    <body>
        <div class="row" style="background-color:rgb(0, 153, 102);padding:20px">
            <span class="title">
                <center>
                    <img src="./images/logo_white_small.png"> New Candidate Test</div>
                </center>
            </span>
        </div>
        <div id="Test"></div>
        @foreach($errors->all() as $error)
                <span class="col-sm-12 text-danger">{{$error}}</span>
        @endforeach
        <ol class="col-xs-12 col-sm-6 col-sm-offset-2">
            <li>
                Install a new
                <a href="http://www.laravel.com" class="btn">
                    Laravel <b><i class="fa fa-external-link"></i></b>
                </a>
                project using
                <a href="https://getcomposer.org/">
                    Composer <b><i class="fa fa-external-link"></i></b>
                </a>
            </li>
            <li>
                Install
                <a href="https://www.npmjs.com/">
                    NPM <b><i class="fa fa-external-link"></i></b>
                </a>
                in the project.
            </li>
            <li>
                Use NPM to install
                <a href="http://www.vuejs.org" class="btn">
                    Vue.js <b><i class="fa fa-external-link"></i></b>
                </a>
            </li>
            <li>
                <i><b>Optional but recommended</b></i><br>
                Use NPM to install
                <a href="https://github.com/vuejs/vue-resource" class="btn">
                    vue-resource <b><i class="fa fa-external-link"></i></b>
                </a> &
                <a href="https://github.com/vuejs/vueify" class="btn">
                    vueify <b><i class="fa fa-external-link"></i></b>
                </a>
            </li>
            <li>
                Include vue.js and any other packages in the project using Laravel's Elixer functions.
            </li>
            <li>
                Build a vue.js component that calls<br>
                <b><p class="bg-info api-link">https://candidate.manulith.com/api/get</p></b>
                <br>The server returns an array of 30 JSON objects.
            </li>
            <li>
                Populate an html table with the data and a form that has checkboxes.
            </li>
            <li>
                POST an input named 'email' with your email address and an array of checked IDs as an object named 'ids' to
                <b><p class="bg-info api-link">https://candidate.manulith.com/api/send</p></b>
            </li>
            <li>
                Change the background color of selected rows after receiving 'success' from the server. Incorrect attempts will return 'failure'.
            </li>
            <li>
                Add the project to github and send a link to zack@makeros.com
            </li>
        </ol>
        <script type="text/javascript" src="./js/bundle.js"></script>
    </body>
</html>
