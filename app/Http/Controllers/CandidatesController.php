<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class CandidatesController extends Controller
{
    public function getData(){
        return \App\SendData::all();
    }

    public function respondData(Request $request){
        if(!$request->ajax()){
            return redirect()->route('home')
                    ->withErrors(['Please use AJAX to communicate with the API.']);           
        }

        if(empty($request->get('email'))){
            \App\ReceiveData::create(['email' => 'No Email Present', 'response' => 'Failure']);
            return ['code' => '500', 'message' => 'No Email Present'];
        }

        $validator = \Validator::make($request->all(), [
            'email' => 'email',
        ]);

        if ($validator->fails()) {
            return json_encode(['code' => '500', 'message' => 'Valid Email Required.']);
        }

        if(is_array($request->get('ids'))){
            foreach($request->get('ids') as $id){
                if(is_numeric($id)){
                    \App\ReceiveData::create(['email' => $request->get('email'), 'response' => 'Success']);
                    return json_encode(['code' => '200', 'message' => 'Success']);
                }else{
                    \App\ReceiveData::create(['email' => $request->get('email'), 'response' => 'Failure - ID less than 0. Received: '.$id]);
                    return json_encode(['code' => '500', 'message' => 'IDs Must Be a Number Greater Than 0']);
                }
            }

        }else{
            \App\ReceiveData::create(['email' => $request->get('email'), 'response' => 'Failure - No ids on request']);
            return json_encode(['code' => '404', 'message' => 'No "ids" Array Present']);
        }
    }
}
