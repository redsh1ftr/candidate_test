<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => function () {
    return view('welcome');
}]);

Route::group(['middleware' => 'cors'], function(){
    Route::get('api/get', ['as' => 'api.get', 'uses' => 'CandidatesController@getData']);
    Route::post('api/send', ['as' => 'api.send', 'uses' => 'CandidatesController@respondData']);
});
